:- consult(golog).

:- op(150, xfx, user:(←)).
:- op(100, fx, user:(¬)).

%! golog_explain(+P, -Explanation)
%
% Generate a logical explanation for P.
%
% @arg P either *poss(A, Sit)* or *holds(Fluent, Sit)*
% @arg Explanation a logical expression denoting why P is true or not.
%
% Explanation for `poss(A, Sit)` returns the simplest logical explanation
% for why that Action is deemed to be possible in Sit, this may include
% unground terms, particularly if using Golog in conjunction with CLP.
%
% Explanation for `holds(Fluent, Sit)` is rudimentary, only finding the
% action that caused the Fluent to hold and ignoring any culmination of
% actions that contributed to it, such as in the case of a bank balance
% being the culmination of multiple deposits and withdrawals.
golog_explain(poss(A, S), Explanation):-
    explainPoss(A, S, Explanation).

golog_explain(holds(F, S), Explanation) :-
    (restoreSitArg(F, _, _) ; F = -Fl, restoreSitArg(Fl, _, _)),
    ( holds(F, S), explainCause(F, S, Explanation)
    ; \+ holds(F, S), Explanation = ¬holds(F, S)
    ).

explainPoss(A, S, poss(A, S) ← Proof) :-
    action(A, Poss),
    explainHolds(Poss, S, Proof).
explainPoss(A, S, ¬poss(A, S) ← Proof) :-
    action(A, Poss),
    explainHolds(-Poss, S, Proof).

explainCause(F, s0, F ← s0) :-
    holds(F, s0).
explainCause(F, do(A, S), F ← A) :-
    holds(F, do(A, S)),
    \+ holds(F, S).
explainCause(F, do(_, S), F ← A) :-
    holds(F, S),
    explainCause(F, S, F ← A).

explainHolds(P & Q, S, Proof1 & Proof2) :-
    explainHolds(P, S, Proof1), explainHolds(Q, S, Proof2).
explainHolds(P v Q, S, Proof) :-
    explainHolds(P, S, Proof) ; explainHolds(Q, S, Proof).
explainHolds(P => Q, S, Proof) :- explainHolds(-P v Q, S, Proof).
explainHolds(P <=> Q, S, Proof) :- explainHolds((P => Q) & (Q => P), S, Proof).
explainHolds(-(-P), S, Proof) :- explainHolds(P, S, Proof).
explainHolds(-(P & Q), S, Proof) :- explainHolds(-P v -Q, S, Proof).
explainHolds(-(P v Q), S, Proof) :- explainHolds(-P & -Q, S, Proof).
explainHolds(-(P => Q), S, Proof) :- explainHolds(-(-P v Q), S, Proof).
explainHolds(-(P <=> Q), S, Proof) :- explainHolds(-((P => Q) & (Q => P)), S, Proof).
explainHolds(-all(V, P), S, Proof) :- explainHolds(some(V, -P), S, Proof).
explainHolds(-some(V, P), S, ¬some(V, P)) :- \+ holds(some(V, P), S).
explainHolds(-P, S, ¬P) :- isAtom(P), \+ holds(P, S).
explainHolds(all(V, P), S, Proof) :- explainHolds(-some(V, -P), S, Proof).
explainHolds(some(V, P), S, Proof) :- sub(V, _, P, P1), explainHolds(P1, S, Proof).

explainHolds(A, S, A) :- restoreSitArg(A, S, F), call(F).
explainHolds(A, S, A) :- \+ restoreSitArg(A, S, _), isAtom(A), call(A).


% Better collecting the rules used, and yielding them to abbreviate... skip transformations.
holds(P & Q, S, Result) --> ['('], holds(P, S, R1), [∩], holds(Q, S, R2), [')'], {and(R1, R2, Result)}.
holds(P v _Q, S, true) --> holds(P, S, true).
holds(_P v Q, S, true) --> holds(Q, S, true).
holds(P v Q, S, false) --> ['('], holds(P, S, false), [∪], holds(Q, S, false), [')'].
holds(P => Q, S, Result) --> holds(-P v Q, S, Result).
holds(P <=> Q, S, Result) --> holds((P => Q) & (Q => P), S, Result).
holds(-(-P), S, Result) --> holds(P, S, Result).
holds(-(P & Q), S, Result) --> holds(-P v -Q, S, Result).
holds(-(P v Q), S, Result) --> holds(-P & -Q, S, Result).
holds(-(P => Q), S, Result) --> holds(-(-P v Q), S, Result).
holds(-(P <=> Q), S, Result) --> holds(-((P => Q) & (Q => P)), S, Result).
holds(-all(V, P), S, Result) --> holds(some(V, -P), S, Result).
holds(-some(V, P), S, Result) --> holds(some(V, P), S, R1), {not(R1, Result)}.
holds(-P, S, Result) --> {isAtom(P)}, holds(P, S, R1), {not(R1, Result)}.
holds(all(V, P), S, Result) --> holds(-some(V, -P), S, Result).
holds(some(V, P), S, Result) --> {sub(V, _, P, P1)}, holds(P1, S, Result).
holds(A, S, Result) -->
    { isAtom(A),
        ( explainCause(A, S, E), Result = true
        ; explainCause(-A, S, E), Result = false
        ), term_to_atom(E, Exp)
    },
    [Exp].

and(true, true, true).
and(_, false, false).
and(false, _, false).

not(true, false).
not(false, true).

ask(P, S) :-
    phrase(holds(P, S, R), E),
    format("~w.~n", R),
    atomic_list_concat(E, ' ', Out),
    writeln(Out), nl.
