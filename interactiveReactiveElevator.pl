
   %  A Reactive Elevator Controller with Interactively
   %              Generated Exogenous Actions

:- consult(rgolog).

:- discontiguous atFloor/2, temp/2.

% Actions

action(goUp, atFloor(N) & topFloor(T) & N < T).
action(goDown, atFloor(N) & firstFloor(F) & F < N).
action(resetButton(N), atFloor(N) & buttonOn(N)).
action(toggleFan, true).
action(ringAlarm, fire).
action(startFire, -fire).
action(endFire, fire).
action(callElevator(N), -buttonOn(N)).
action(changeTemp, true).
action(resetAlarm, alarmOn).
action(wait, true).


% Golog Procedures

proc(control, wait : while(some(n,buttonOn(n)),
                           pi(n, ?(buttonOn(n)) : serveFloor(n)))).

proc(serveFloor(N), while(-atFloor(N), if(aboveFloor(N), goDown, goUp)) :
                    resetButton(N)).

proc(rules, pi(c, pi(r, ?(rule(c, r)) : ?(c) : r)) # no_op).

/*
proc(rules, ?(fire & -alarmOn) : ringAlarm : while(alarmOn,wait) #
            ?(tooHot & -fan) : toggleFan #
            ?(tooCold & fan) : toggleFan #
             ?(-(fire & -alarmOn v tooHot & -fan v tooCold & fan))).
 */

rule(fire & -alarmOn, ringAlarm : while(alarmOn,wait)).
rule(tooHot & -fan, toggleFan).
rule(tooCold & fan, toggleFan).


% exoTransition under the assumption that at most one exogenous action
% can occur after each control action.

exoTransition(S1,S2) :- requestExogenousAction(E,S1),
                        (E = nil, S2 = S1 ;
                         \+ E = nil, S2 = do(E,S1)).

requestExogenousAction(E,S) :-
           writeln("Enter an exogenous action, or nil."), read(E1),
  %  IF exogenous action is nil, or is possible THEN no problem
               ((E1 = nil ; poss(E1,S)) -> E = E1 ;
  %  ELSE print error message, and try again.
               write(">> Action not possible. Try again."), nl,
               requestExogenousAction(E,S)).

% Successor State Axioms for Primitive Fluents.

buttonOn(N,do(A,S)) :- A = callElevator(N) ;
                       buttonOn(N,S), \+ A = resetButton(N).
fire(do(A,S)) :- A = startFire ; \+ A = endFire, fire(S).
fan(do(A,S)) :- A = toggleFan, \+ fan(S) ; \+ A = toggleFan, fan(S).
atFloor(N,do(A,S)) :- A = goDown, atFloor(M,S), N is M - 1 ;
                      A = goUp, atFloor(M,S), N is M + 1 ;
                      \+ A = goDown, \+ A = goUp, atFloor(N,S).
alarmOn(do(A,S)) :- A = ringAlarm ; \+ A = resetAlarm, alarmOn(S).
temp(T,do(A,S)) :- A = changeTemp, temp(T1,S), (\+ fan(S), T is T1 + 1 ;
                                                fan(S), T is T1 - 1) ;
                   \+ A = changeTemp, temp(T,S).
% Abbreviations

tooHot(S) :- temp(T,S), T > 3.
tooCold(S) :- temp(T,S), T < -3.
aboveFloor(N,S) :- atFloor(M,S), N < M.

% Initial Situation.

atFloor(1,s0).  temp(0,s0).  topFloor(6).  firstFloor(1).

% Restore suppressed situation arguments.

restoreSitArg(tooCold,S,tooCold(S)). restoreSitArg(fire,S,fire(S)).
restoreSitArg(buttonOn(N),S,buttonOn(N,S)). restoreSitArg(fan,S,fan(S)).
restoreSitArg(tooHot,S,tooHot(S)).
restoreSitArg(atFloor(N),S,atFloor(N,S)).
restoreSitArg(alarmOn,S,alarmOn(S)). restoreSitArg(temp(T),S,temp(T,S)).
restoreSitArg(aboveFloor(N),S,aboveFloor(N,S)).
restoreSitArg(requestExogenousAction(E),S,requestExogenousAction(E,S)).

elevator :- doR(control,rules,s0,S), prettyPrintSituation(S).

prettyPrintSituation(S) :- makeActionList(S,Alist), nl,
                           write(Alist), nl.

makeActionList(s0,[]).
makeActionList(do(A,S),L) :- makeActionList(S,L1), append(L1,[A],L).
