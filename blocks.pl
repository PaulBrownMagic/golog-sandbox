:- consult(golog).

action(move(X, Y), dif(X, Y) & clear(X) & clear(Y)).
action(moveToTable(X), clear(X) & -ontable(X)).

clear(a, s0).
clear(d, s0).
clear(X, do(A, S)) :-
    ( A = move(Y, _Z) ; A = moveToTable(Y) ), on(Y, X, S) ;
    clear(X, S), \+ A = move(_Y, X).

on(a, b, s0).
on(b, c, s0).
on(X, Y, do(A, S)) :-
    A = move(X, Y) ;
    on(X, Y, S), \+ A = move(X, _Z).

t_on(A, B, S) :-
    on(A, B, S) ;
    on(A, C, S),
    t_on(C, B, S).

ontable(c, s0).
ontable(d, s0).
ontable(X, do(A, S)) :-
    A = moveToTable(X) ;
    ontable(X, S), \+ A = move(X, _Y).

restoreSitArg(clear(X), S, clear(X, S)).
restoreSitArg(on(A, B), S, on(A, B, S)).
restoreSitArg(t_on(A, B), S, t_on(A, B, S)).
restoreSitArg(ontable(X), S, ontable(X, S)).

proc(flattenTower(X),
    ?(ontable(X)) #
    pi(n, ?(on(X, n)) : moveToTable(X) : flattenTower(n))
).

proc(reverseTower(X),
    ?(ontable(X)) #
    pi(n, ?(on(X, n)) : moveToTable(X) : reverse_(n, X))
).

proc(reverse_(Block, Last),
    ( ?(ontable(Block)) : move(Block, Last) ) #
    pi(n, ?(on(Block, n)) : move(Block, Last) : reverse_(n, Block))
).

proc(appendTowers(A, B),
    ( ?(ontable(A)) : move(A, B) ) #
    pi(n, ?(on(A, n)) : move(A, B) : appendTowers(n, A))
).

proc(ordAppendTowers(A, B),
    (?(ontable(A)) : move(A, B) ) #
    pi(base, ?(t_on(A, base) & ontable(base)) :
        reverseTower(A) :
        appendTowers(base, B)
    )
).
