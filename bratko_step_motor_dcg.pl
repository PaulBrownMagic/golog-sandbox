% As per Bratko's motor example DCG in Prolog for AI, around page 566
% But in the language of situation calculus

% DCG for sequence of up, down moves
sit(do(A, s0)) --> action_(A).
sit(do(A, Sit)) --> action_(A), sit(Sit).
action_(step(up)) --> [up].
action_(step(down)) --> [down].
action_(grasp) --> [grasp].
action_(drop) --> [drop].

% getting meaning in Prolog
p_distance(do(A, Sit), Dist) :-
    p_distance(A, D1),
    p_distance(Sit, D2),
    Dist is D1 + D2.

p_distance(do(A, s0), Dist) :-
    p_distance(A, Dist).

p_distance(step(up), 1).
p_distance(step(down), -1).
p_distance(A, 0) :- \+ A = step(_).

?- phrase(sit(Sit), [grasp, up, down, up, up, drop]),
   p_distance(Sit, Dist),
   writeln("Simple Moves and meaning:"),
   write("Sit: "), writeln(Sit),
   write("Dist: "), writeln(Dist), nl.

% interleving meaning
distance(D) --> action(D).
distance(D) --> action(D1), distance(D2), {D is D1 + D2}.
action(1) --> [up].
action(-1) --> [down].
action(0) --> [grasp].
action(0) --> [drop].

?- phrase(distance(Dist), [grasp, up, down, up, up, down]),
   writeln("Interleved Moves and meaning"),
   write("Dist: "), writeln(Dist), nl.
