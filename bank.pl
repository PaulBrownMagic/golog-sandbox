:- module(bank,
    [ poss/2
    , holds/2
    , do/3
    , proc/2
    , action/2
    , golog_explain/2
    ]
).

:- consult(explanations).

action(create_account(X), -exists_account(X)).
action(delete_account(X), exists_account(X) & balance(X, 0)).
action(deposit(X, _), exists_account(X)).
action(withdraw(Account, Money), balance(Account, B) & B >= Money).

proc(open_account(X, Y), create_account(X) : deposit(X, Y)).
proc(close_account(X), ?(balance(X, Y)) : withdraw(X, Y) : delete_account(X)).


exists_account(X, do(A, S)) :-
    exists_account_effect(+A, X) ;
    exists_account(X, S), \+ exists_account_effect(-A, X).

exists_account_effect(+create_account(X), X).
exists_account_effect(-delete_account(X), X).

balance(X, _, do(delete_account(X), _)) :- !, fail.
balance(X, 0, do(create_account(X), _)).
balance(X, V, do(A, S)) :-
    balance(X, N, S), balance_effect(A, X, N, M), V is M.
balance(X, V, do(A, S)) :-
    \+ balance_effect(A, X, _, _),
    balance(X, V, S).

balance_effect(deposit(A, B), A, N, N+B).
balance_effect(withdraw(A, B), A, N, N-B).

restoreSitArg(exists_account(X), S, exists_account(X, S)).
restoreSitArg(balance(X, B), S, balance(X, B, S)).
