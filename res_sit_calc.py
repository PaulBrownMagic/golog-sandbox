class Situation(dict):
    pass

def s0():
    return Situation(
        distance = 0,
        holding = False
    )

def result(A, S):
    if ACTIONS[A].poss(S):
        ACTIONS[A].do(S)
        return S
    else:
        raise ValueError(f"{A} is not possible in {S}")

class Action:
   pass 

class Up(Action):
    def do(self, S):
        S['distance'] += 1
    def poss(self, S):
        return True

class Down(Action):
    def do(self, S):
        S['distance'] -= 1
    def poss(self, S):
        return True

class Grasp(Action):
    def do(self, S):
        S['holding'] = True
    def poss(self, S):
        return S['holding'] == False

class Drop(Action):
    def do(self, S):
        S['holding'] = False
    def poss(self, S):
        return S['holding'] == True

ACTIONS = dict(
    up = Up(),
    down = Down(),
    grasp = Grasp(),
    drop = Drop()
    )

def poss(S):
    return (a for a, c in ACTIONS.items() if c.poss(S))

def is_poss(A, S):
    return A in poss(S)

def holds(F, S):
    return S[F]
