/* <module> Situation Persistency Manager

The Situation Manager handles the changes to the KB
situations. More than one situation can be stored, similar
to a RDBS having multiple tables, to allow for different streams
of situations to be reasoned over separately.

Each Situation needs to be identified by a unique ID, this
can be provided to `create_situation/1` (or /2), or if ID
is not ground then one will be provided by `uuid/1`.

Each public CRUD operation operates within a mutex for thread-safety.

`update_situation/2` Uses Golog's `do/3` to determine the new situation,
if using RGolog or some other variation on the interpreter, do/3 needs
to be provided as a proxy: RGolog e.g.:

```
do(A, S, NS) :-
    proc(rules, _),
    doR(A, rules, S, NS).
```

Furthermore, the Golog interpreter requires access to all
predicates it may call, such as `proc/2` and `restoreSitArg/3`.

@author Paul Brown

*/

:- module(situation_persistency,
    [ situations_db_attach/1
    , use_golog_interpreter/1
    , get_situation/2
    , update_situation/2
    , create_situation/1
    , create_situation/2
    , delete_situation/1
    ]
).

:- use_module(library(persistency)).

:- persistent situation(id:atom, situation:any).

%! situations_db_attach(+File)
% attach the File as the persistent
% storage for situations
situations_db_attach(File) :-
    ground(File),
    db_attach(File, []).

%! use_golog_interpreter(Interpreter)
% consult the provided golog interpreter
% to reason over situations.
use_golog_interpreter(G) :-
    ensure_loaded(G).

%! get_situation(+ID, ?Sit)
%
% Sit is the situation with the given ID.
get_situation(ID, Sit) :-
    ground(ID),
    with_mutex(situation_db, situation(ID, Sit)).

%! update_situation(+ID, +ToDo)
%
% Update the situation identified by ID by
% doing the process or action unified with ToDo.
update_situation(ID, ToDo) :-
    ground(ID), ground(ToDo),
    with_mutex(situation_db,
        update_situation_(ID, ToDo)
    ).

%! update_situation_(ID, ToDo)
% Private helper for update_situation,
% does not include ground args or
% mutex constraints.
update_situation_(ID, ToDo) :-
    situation(ID, Sit),
    do(ToDo, Sit, NewSit),
    retractall_situation(ID, _),
    assert_situation(ID, NewSit),
    db_sync(gc).

%! set_situation_(ID, Sit)
% used as: Private helper for create_situation
% Clobbers any situation with existing
% ID and changes the situation to Sit.
% Don't use for updates as the mutex
% would only cover part of the process.
set_situation_(ID, Sit) :-
    with_mutex(situation_db,
        ( retractall_situation(ID, _)
        , assert_situation(ID, Sit)
        )
    ).

%! create_situation(?ID)
%
% create a new situation in S0 with ID.
create_situation(ID) :-
    (ground(ID) ; uuid(ID)),
    set_situation(ID, s0).

%! create_situation(?ID, +Sit)
%
% create a new situation with Sit as
% the initial situation.
create_situation(ID, Sit) :-
    ground(Sit), (ground(ID) ; uuid(ID)),
    set_situation(ID, Sit).

%! delete_situation(+ID)
%
% Delete the situation identified by ID from
% the database.
delete_situation(ID) :-
    ground(ID),
    with_mutex(situation_db,
        retractall_situation(ID, _)
    ).
