:- consult(explanations).
:- use_module(library(clpfd)).

%! action(?Action, ?Poss)
% Action is an action that is defined to be possible when Poss holds
action(turnoff(N), on(N)).
action(open_door, true).
action(close_door, true).
action(up(N), some(m, currentFloor(m) & m #< N & N #< 7)).
action(down(N), some(m, currentFloor(m) & m #> N & N #> 0)).

%! proc(?Proc, ?Steps)
% Proc is a process that is defined by Steps
proc(goFloor(N), ?(currentFloor(N)) # up(N) # down(N)).
proc(serve(N), goFloor(N) : turnoff(N) : open_door : close_door).
proc(serveAfloor, pi(n, ?(nextFloor(n)) : serve(n))).
proc(park, if(currentFloor(1), open_door, down(1) : open_door)).

proc(control, while(some(n, on(n)), serveAfloor) : park).


%! currentFloor(?N, ?Sit)
% currentFloor is a Fluent
%
% @arg N the number of the current floor
% @arg Sit the situation for which the currentFloor fluent holds
currentFloor(4, s0).
currentFloor(N, do(A, S)) :-
    positiveEffectAxiom(currentFloor, A, N) ;
    \+ positiveEffectAxiom(currentFloor, A, _), currentFloor(N, S).


%! on(?N, ?Sit)
% on is a Fluent
%
% @arg N the number of the floor for which the call switch is on
% @arg Sit the situation for which the on fluent holds
on(3, s0).
on(5, s0).
on(N, do(A, S)) :-
    on(N, S), \+ negativeEffectAxiom(on, A, N).

%! positiveEffectAxiom(+Fluent, +A, -Payload)
% A is an action that causes Payload to hold for Fluent
positiveEffectAxiom(currentFloor, up(N), N).
positiveEffectAxiom(currentFloor, down(N), N).

%! negativeEffectAxiom(+Fluent, +A, -Payload)
% A is an action that causes Payload to *not* hold for Fluent
negativeEffectAxiom(on, turnoff(N), N).

%! nextFloor(?N, ?Sit)
% A helper predicate to determine the
% next floor to be served
nextFloor(N, S) :- on(N, S).

restoreSitArg(on(X), S, on(X, S)).
restoreSitArg(currentFloor(X), S, currentFloor(X, S)).
restoreSitArg(nextFloor(X), S, nextFloor(X, S)).
