/** <module> Situations

Helper predicates for manipulating Golog situations.

@author Paul Brown
*/

:- consult(golog).

%! hasPrior(?Sit, ?PriorSit)
% True iff Sit contains PriorSit within it
% Unifies a final variable situation with s0
hasPrior(do(_A, s0), s0) :- !.
hasPrior(do(_A, S), S).
hasPrior(do(_A, S), PS) :-
    hasPrior(S, PS).

%! executable(?Sit)
% Situation is executable iff every action
% is possible in the situation that it is done.
% Can be used for validation and generation for
% iterative deepening.
executable(s0).
executable(do(A, S)) :-
    executable(S),
    poss(A, S).

%! situationLength(?Sit, ?L)
% The length of a situation is the number
% of actions done in that situation:
% ```
% ?- situationLength(s0, 0).
% true.
% ?- situationLength(do(foo, s0), 1).
% true.
% ```
situationLength(Sit, L) :-
    situationLength(Sit, 0, L), !.
situationLength(s0, L, L).
situationLength(do(_A, S), Acc, L) :-
    succ(Acc, NewAcc),
    situationLength(S, NewAcc, L).

%! pp_sit(+Sit)
%  pretty prints a situation for easier reading
pp_sit(s0) :-
    format("s0 →~n").
pp_sit(do(A, S)) :-
    pp_sit(S),
    format("~w →~n", [A]).
%! pp_step(+Sit)
% pretty print but uses "or" between actions
% to allow user to step through actions.
pp_step(s0) :-
    format("s0 →~n").
pp_step(do(A, S)) :-
    pp_step(S) ;
    format("~n~w →~n", [A]).

%! situationMember(?A, ?Sit)
% A is an action done in that Situation,
% same as member(A, [pickup, goto(place), dropoff]).
situationMember(A, do(H, S)) :-
    sitmem_(S, A, H).
sitmem_(_, A, A).
sitmem_(do(H, S), A, _) :-
    sitmem_(S, A, H).

%! situationPrefix(?Prefix, +Sit)
% Prefix is the latter part of Sit
% with a variable for the remaining
% situation:
% ```
% ?- situationPrefix(P, do(foo, do(bar, s0))).
% P = do(foo, _) ;
% ```
situationPrefix(PS, S) :-
    ground(S), situationPrefix_(PS, S).
situationPrefix_(do(A, _), do(A, _)).
situationPrefix_(do(A, SP), do(A, S)) :-
    situationPrefix(SP, S).


%! adjacentActions(?A, ?B, ?Sit)
% True iff action A immediately precedes
% action B in a situation. i.e. do(A, do(B, _))
% or do(_, do(A, do(B, _))) and so on.
adjacentActions(A, B, do(A, do(B, _))).
adjacentActions(A, B, do(_, S)) :-
    adjacentActions(A, B, S).

%! sameLength(?S1, ?S2)
% True iff S1 and S2 contain the same
% number of actions
sameLength(s0, s0).
sameLength(do(_, S1), do(_, S2)) :-
    sameLength(S1, S2).

%! longerShorter(?S1, ?S2)
% True iff S1 contains more actions than S2
longerShorter(do(_, _), s0).
longerShorter(do(_, S1), do(_, S2)) :-
    longerShorter(S1, S2).

%! situationList(?Sit, ?List)
% equivalent to situationList(Sit, List, [ltr]).
situationList(Sit, [s0|T]) :-
    sitList(Sit, [], [s0|T]), !.

%! situationList(?Sit, ?List, +Opts)
% Transform the situation structure into
% a list structure.
%
% Available Options:
% - `direction(Dir)`: Dir can be either *ltr* (**default**) or *rtl*. *ltr* reads s0 at the head and accends to the most recent action, *rtl* reads most recent action as the head and decends to s0.
% - `no_s0`: optional, don't include s0 in the list.
situationList(Sit, List, Opts) :-
    \+ memberchk(direction(rtl), Opts),
    (memberchk(no_s0, Opts) -> Out = [s0|List] ; Out = List, List = [s0|_]),
    sitList(Sit, [], Out), !.
situationList(Sit, List, Opts) :-
    memberchk(direction(rtl), Opts),
    ( memberchk(no_s0, Opts) ->
      O = no_s0 ;
      O = with_s0, (ground(List) , last(List, s0) ; \+ ground(List))
    ),
    holeSitList(Sit, X-X, List, O), !.

% sitList(Sit, Acc, List)
% ltr helper predicate for situationList/2,3
sitList(s0, Acc, [s0 | Acc]).
sitList(do(A, S), Acc, Out) :-
    sitList(S, [A|Acc], Out).

% holeSitList(Sit, Acc, List, s0_Opt)
% rtl helper predicate for situationList/3
holeSitList(s0, Acc-[s0], Acc, with_s0).
holeSitList(s0, Acc-[], Acc, no_s0).
holeSitList(do(A, S), Acc-T, List, Opt) :-
    T = [A|H],
    holeSitList(S, Acc-H, List, Opt).

%! appendSit(?Sit1, ?Sit2, ?Sit3)
% as per `append/3` but applied to
% situations: True iff Sit1 Appended to
% Sit2 is Sit3. Sit1 actions occur after
% those in Sit2
appendSit(s0, Sit, Sit).
appendSit(do(A, S), Ss, do(A, As)) :-
    appendSit(S, Ss, As).

