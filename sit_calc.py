def s0(): return ["s0"]

def do(a, s):
    if a in ACTIONS and is_poss(a, s):
        s.append(a)
        return s
    raise ValueError(f"{a} is not possible in {s}")
    
def is_poss(a, s):
    return a in poss(s)

def poss(s):
    return (a for a, p in ACTIONS.items() if p(s))

def holds(f, s):
    return FLUENTS[f](s)

def all_holding(s):
    return {k: f(s) for k, f in FLUENTS.items()}

ACTIONS = dict(
    up = lambda _: True,
    down = lambda _: True,
    grasp = lambda s: not holding(s),
    drop = lambda s: holding(s)
)

def distance(s):
    return sum(map(lambda a: 1 if a == "up" else -1, filter(lambda a: a in ["up", "down"], s)))

def holding(s):
    if s == s0(): return False
    return (s[-1] == "grasp") or (holding(s[:-1]) and not s[-1] == "drop")

FLUENTS = dict(
    distance = distance,
    holding = holding,
    )
