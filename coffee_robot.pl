:- consult(temporal_golog).

proc(deliverCoffee(T),
    ?(now(NT) & NT #=< T) :
    (?(all(p,all(t1,all(t2, wantsCoffee(p, t1, t2) => hasCoffee(p)))))
    #
    ?(robotLocation(Rloc)) :
        if(Rloc = cm,
            deliverOneCoffee(T),
            goto(cm, T) : ?(now(DOT)) : deliverOneCoffee(DOT)) :
        ?(now(RDT)) : deliverCoffee(RDT)
    )).

proc(deliverOneCoffee(T),
        ?(wantsCoffee(P, T1, T2) & -hasCoffee(P) & Wait #>= 0 &
        travelTime(cm, office(P), Travtime) &
        D #= T + Wait + Travtime &
        T1 #=< D & D #=< T2 & PCT #= T + Wait) :
        pickupCoffee(PCT) :
        ?(now(NT1)) : goto(office(P), NT1) :
        ?(now(NT2)) : giveCoffee(P, NT2)).

proc(goto(L, T),
    ?(robotLocation(Rloc)) :
    ?(travelTime(Rloc, L, Deltat)) :
    goBetween(Rloc, L, Deltat, T)).

proc(goBetween(Loc1, Loc2, Delta, T),
    startGo(Loc1, Loc2, T) :
    ?(ET #= T + Delta) : endGo(Loc1, Loc2, ET)).

action(pickupCoffee(_T), -holdingCoffee & robotLocation(cm)).
action(giveCoffee(Person, _T), holdingCoffee & robotLocation(office(Person))).
action(startGo(Loc1, Loc2, _T), -going(_, _) & dif(Loc1, Loc2) & robotLocation(Loc1)).
action(endGo(Loc1, Loc2, _T), going(Loc1, Loc2)).

hasCoffee(Person, do(A, S)) :-
    A = giveCoffee(Person, _T) ;
    hasCoffee(Person, S).

robotLocation(cm, s0).
robotLocation(Loc, do(A, S)) :-
    A = endGo(_, Loc, _T) ;
    robotLocation(Loc, S), \+ A = endGo(_, _, _T).

going(Loc1, Loc2, do(A, S)) :-
    A = startGo(Loc1, Loc2, _T) ;
    going(Loc1, Loc2, S), \+ A = endGo(Loc1, Loc2, _T).

holdingCoffee(do(A, S)) :-
    A = pickupCoffee(_T) ;
    holdingCoffee(S), \+ A = giveCoffee(_Person, _T).

start(s0, 0).

wantsCoffee(sue, 140, 160).
wantsCoffee(bill, 100, 110).
wantsCoffee(joe, 90, 100).
wantsCoffee(mary, 130, 170).

travelTime_(cm, office(sue), 15).
travelTime_(cm, office(mary), 10).
travelTime_(cm, office(joe), 10).
travelTime_(cm, office(bill), 8).

travelTime(L, L, 0).
travelTime(L1, L2, T) :-
    travelTime_(L1, L2, T) ; travelTime_(L2, L1, T).

time(pickupCoffee(T), T).
time(giveCoffee(_P, T), T).
time(startGo(_, _, T), T).
time(endGo(_, _, T), T).

chooseTimes(s0).
chooseTimes(do(A, S)) :- chooseTimes(S), time(A, T), rmin(T).

now(S, T) :- start(S, T).
restoreSitArg(now(T), S, now(S, T)).
restoreSitArg(hasCoffee(P), S, hasCoffee(P, S)).
restoreSitArg(robotLocation(L), S, robotLocation(L, S)).
restoreSitArg(going(L1, L2), S, going(L1, L2, S)).
restoreSitArg(holdingCoffee, S, holdingCoffee(S)).

rmin(T) :-
    once(labeling([min(T)], [T])).

pp_sit(s0) :-
    format("~ns0 →~n").
pp_sit(do(A, S)) :-
    pp_sit(S),
    format("~w →~n", [A]).

coffeeDelivery(T) :-
    do(deliverCoffee(T), s0, S), chooseTimes(S),
    pp_sit(S), askForMore.

askForMore :- format("~nMore? |: "), read(n).
