# Golog Sandbox

This repository contains a *useful* collection of Golog related files
including examples from [Knowledge In Action by Raymond
Reiter](https://amzn.to/2YkI5XE).

To run this code requires the interpreters available from
the [KIA's homepage](http://www.cs.toronto.edu/cogrobo/kia/),
these may require modification to run. Licence doesn't permit
redistribution of modified interpreter, so I can't my versions
I'm afraid.

Notable files:

- `situations.pl`: Some useful manipulation predicates
- `explanations.pl`: Trying to explain holds/2 and poss/2 conclusions
